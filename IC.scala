import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.spark.sql.SQLContext._
import org.apache.spark.rdd.RDD 					                             
import org.apache.spark._
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{
    StructType, StructField, StringType, IntegerType, FloatType}
import scala.collection.mutable.WrappedArray
import scala.collection.mutable.ListBuffer


object IC_Calculation {

   
   val explodedSchema = StructType(								
			StructField("Drug", StringType, false) :: 
			StructField("Reaction", StringType,false) :: Nil) 
	 	


    def main(args: Array[String]) {
		  									                                       
				val conf = new SparkConf()
                .setAppName("IC_CALCULATIONS")
                .set("spark.kryo.registrator", 
                "com.art.spark.AvroKyroRegistrator"
                )
            
				val sc = new SparkContext(conf)						
  
				val sqlContext = new org.apache.spark.sql.SQLContext(sc)                                               


    		val UMC_DataFrame = sqlContext.read.format("json").
            load("file:///usr/local/spark/spark-2.0.0-bin-hadoop2.7/data/UMC")    
          
          
				val drugs_and_reactions = UMC_DataFrame.select(
                "UMCReport.Patient.Drugs.MedicinalProduct",
                "UMCReport.Patient.Reactions.ReactionMeddraPt"
                )
	
				drugs_and_reactions.registerTempTable("drugs_and_reactions")

				def log2(x: Double): Double = scala.math.log(x) / scala.math.log(2)		
				
                
     		def exploreReactionDrug(x: Row) :  ListBuffer[Row] = {   
        
					val drugReactionPairs = ListBuffer.empty[Row]
        
					val possibleDrugs:Seq[String] = x.get(0).asInstanceOf[Seq[String]]
        
					val possibleReactions:Seq[String] = x.get(1).asInstanceOf[Seq[String]]
        
					val MakepossibleDrugsToArray = possibleDrugs.toArray
        
					val MakepossibleReactionsToArray = possibleReactions.toArray
        

					for(Drug <- MakepossibleDrugsToArray) {
						for (Reaction <- MakepossibleReactionsToArray) {
            			drugReactionPairs += Row.apply(Drug, Reaction) 				
        				}	
					}
        
   				return drugReactionPairs
     		}
	
				val explodedRDD = drugs_and_reactions.rdd
                .flatMap(x => exploreReactionDrug(x))           

				val sqlDataFrame_from_explodedRDD = sqlContext.
                createDataFrame(explodedRDD, explodedSchema)
    
				import sqlContext.implicits._
    
				sqlDataFrame_from_explodedRDD.registerTempTable("AdverseDrugReactionsPairCombinations") 
	
				val all = sqlContext.sql("
                SELECT COUNT(*)
                AS num_reactions_to_drug,Drug,Reaction
                FROM AdverseDrugReactionsPairCombinations 
                GROUP BY Drug,Reaction"
                )
               	
				all.registerTempTable("AdverseDrugReactionsPairsSelection")   
                
				val drugers = sqlContext.sql("
                SELECT COUNT(*)
                AS num_drugusers,Drug
                FROM AdverseDrugReactionsPairsSelection
                GROUP BY Drug"
                )
                
				drugers.registerTempTable("drugers")		   
	
				val reactionists = sqlContext.sql("
                SELECT sum(num_reactions_to_drug)
                AS num_reacts, Reaction
                FROM AdverseDrugReactionsPairsSelection
                GROUP BY Reaction"
                )
                
				reactionists.registerTempTable("reactionists") 


				val numpepes = sqlContext.sql("
                SELECT COUNT(*)
                AS num_entries
                FROM AdverseDrugReactionsPairCombinations"
                )   
                
				numpepes.registerTempTable("numpepes")
						


			  val ICcolumnsDF = sqlContext.sql("
            SELECT dp.Drug,dp.Reaction,dp.num_reactions_to_drug
            AS drug_and_reaction,d.num_drugusers - dp.num_reactions_to_drug
            AS drug_and_noreaction,r.num_reacts - dp.num_reactions_to_drug
            AS nodrug_and_reaction, np.num_entries - (d.num_drugusers - dp.num_reactions_to_drug) - (r.num_reacts - dp.num_reactions_to_drug)
            AS nodrug_and_noreaction FROM AdverseDrugReactionsPairsSelection
            AS dp
            JOIN drugers
            AS d
            ON dp.Drug = d.Drug
            JOIN reactionists
            AS r 
            ON dp.Reaction = r.Reaction
            JOIN numpepes
            AS np"
            )
            
            ICcolumnsDF.registerTempTable("ICcolumns") 


			  val IC = sqlContext.sql("
            SELECT Drug,Reaction,
            Log2(
            (drug_and_reaction+0.5)
            /
            ((((drug_and_reaction + nodrug_and_reaction)
            /
            (
            drug_and_reaction + drug_and_noreaction + nodrug_and_reaction + nodrug_and_noreaction))
            *
            (drug_and_reaction + drug_and_noreaction))
            +0.5)
            )
            AS Icnumber
            FROM ICcolumns"
            )
            
            
		     val storeToVirtualMemory = IC.rdd.
         saveAsTextFile("hdfs:///usr/local/hadoop/data/namenode")
   
  }

}  

